from abc import ABC, abstractmethod

class ReadFileABC(ABC):
	"""Interfaz para leer archivos"""
	
	@abstractmethod
	def read_file(self, file) -> dict:
		pass

	@abstractmethod
	def info(self) -> str:
		pass