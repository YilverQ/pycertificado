from .ReadFileABC import ReadFileABC
import csv

class ReadFileCSV(ReadFileABC):
	"""Clase para leer archivos CSV"""
	def read_file(self, file : str) -> dict:
		with open(file) as csvfile:
			#Recorremos el objeto y retornamos un diccionario
			return [dict(row) for row in csv.DictReader(csvfile)]

	def info(self) -> str:
		return "Leer archivo de tipo CSV"
