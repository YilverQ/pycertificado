from .ReadFileABC import ReadFileABC

class ReadFileXLS(ReadFileABC):
	"""Clase para leer archivos XLS"""
	def read_file(self, file) -> dict:
		return "Leemos un archivo XLS"

	def info(self) -> str:
		return "Leer archivo de tipo XLS"