from .ReadFileABC import ReadFileABC
from .ReadFileCSV import ReadFileCSV
from .ReadFileXLS import ReadFileXLS
from .ReadFileODS import ReadFileODS

def get_type_file(type_file : str) -> ReadFileABC:
	type_files_class = {
		"CSV" : ReadFileCSV,
		"XLS" : ReadFileXLS,
		"ODS" : ReadFileODS
	}

	if type_file in type_files_class:
		return type_files_class[type_file]()

	return "Tipo de archivo no admitido"