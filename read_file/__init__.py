from .ReadFileCSV import ReadFileCSV
from .ReadFileODS import ReadFileODS
from .ReadFileXLS import ReadFileXLS
from .get_type_file import get_type_file