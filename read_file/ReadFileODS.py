from .ReadFileABC import ReadFileABC

class ReadFileODS(ReadFileABC):
	"""Clase para leer archivos ODS"""
	def read_file(self, file) -> dict:
		return "Leemos un archivo ODS"

	def info(self) -> str:
		return "Leer archivo de tipo ODS"