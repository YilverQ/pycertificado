#Nos permitirá convertir archivos HTML y CSS en PDF.
from weasyprint import HTML, CSS 
from weasyprint.text.fonts import FontConfiguration #Nos permite agreagar fuentes.
#['FontConfiguration'] Nos permite agreagar fuentes al momento de generar un PDF
font_config = FontConfiguration()

def save_pdf(css_path : str, page_html : str, save_file):
	#Convertimos el archivo HTML a PDF. 
	#Abrimos el archivo CSS.
	file_css = open(css_path, "r")

	#Lee el contenido del archivo
	styles_code = file_css.read()

	#Cierra el archivo
	file_css.close()

	#Objeto de tipo CSS.
	css = CSS(string=styles_code, font_config=font_config)

	#Generamos un PDF a partir de un texto HTML.
	pdf_file = HTML(string=page_html).write_pdf(save_file,
												stylesheets=[css], 
												font_config=font_config)