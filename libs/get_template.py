#Nos permite trabajar con archios HTML.
from jinja2 import Environment, FileSystemLoader 

#Importamos las variables de entorno.
from dotenv import load_dotenv
import os
load_dotenv()

#Cargamos el template en la memoria.
def get_template():
    TEMPLATE_FOLDER = os.environ.get('TEMPLATE_FOLDER')
    TEMPLATE_HTML   = os.environ.get('TEMPLATE_HTML')
    
    #Cargamos los archivos de plantilla desde el directorio de plantilla
    #Enviroment nos permite cargar valores de variables de entorno.
    #FileSystemLoader es una función que carga la carpeta donde están las plantillas html. 
    env = Environment(loader=FileSystemLoader(TEMPLATE_FOLDER))

    template = env.get_template(TEMPLATE_HTML) #Obtenemos el archivo template.
    
    return template