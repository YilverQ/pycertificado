from dotenv import load_dotenv
import os
load_dotenv()


"""
    EJEMPLO:
    os.mkdir("/ruta/completa/carpeta_nueva")
"""

#Creamos una carpeta para almacenar los certificados
def make_folder():
    CERTIFICATES_FOLDER = os.environ.get('CERTIFICATES_FOLDER')
    folder_path = os.getcwd() + "/" + CERTIFICATES_FOLDER
    try:
        # Crea la carpeta
        os.mkdir(CERTIFICATES_FOLDER)
        print(f"Carpeta: {CERTIFICATES_FOLDER}, creada exitosamente")
    except FileExistsError:
        print(f"La carpeta {CERTIFICATES_FOLDER}, ya existe")
        

    return folder_path