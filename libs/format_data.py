#Importamos las variables de entorno.
from dotenv import load_dotenv
import os
load_dotenv()

def format_data(data : dict) -> dict:
	#Ubicación de los archivos.
	TEMPLATE_FOLDER = os.environ.get('TEMPLATE_FOLDER')
	img_path = os.getcwd() + "/" + TEMPLATE_FOLDER + "/" + os.environ.get("IMAGEN_CSS_TEMPLATE")

    #Almacenamos la ubicación de la imagen de estilos. 
	#La imagen de fondo del certificado está formado por: 
	#modeloCertificado + modalidadParticipante.
	imagen_css_template = img_path + f"/{data['model']}" + f"/{data['modality']}.css"
	css_template = img_path + f"/{data['model']}" + f"/estilos.css"

    #Formateamos el número de cédula. 
	#(10000000 -> 10.000.000).
	number = "{0:,}".format(int(data['identification_card'])).replace(",", ".")

	data["identification_card"] = number
	data["imagen_css_template"] = imagen_css_template
	data["css_template"] 		= css_template

	return data