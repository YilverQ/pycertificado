#Librería para generar código qr. 
import qrcode 

#Codifica datos binarios en datos ASCII. 
import base64

#Permite grabar datos binarios en memoria. 
from io import BytesIO

def qr_code_generate(text):
    #Generar un objeto de tipo qrCode.
    qr = qrcode.QRCode(
        version=1, #Versión del código qr.
        error_correction=qrcode.constants.ERROR_CORRECT_L, #nivel de corrección del qr.
        box_size=5, #Tamaño del cuadro qr.
        border=1, #Padding del cuadro qr.
    )

    #Agregamos un texto al qr. 
    qr.add_data(text)

    #Genera el código qr. fit=True es para ajustar el qr al tamaño especificado.
    qr.make(fit=True)
    
    #Crea una imagen del qr. fill_color:color del código, back_color:color de fondo.
    img = qr.make_image(fill_color="black", back_color="white")


    #Espacio en memoria disponible para almacenar la imagen qr.
    buffered = BytesIO()
    
    #Guarda la imagen en formato PNG en la memoria.
    img.save(buffered, format="PNG")
    
    #Convierte la imagen en base64.
    qr_image_base64 = base64.b64encode(buffered.getvalue()).decode()

    return qr_image_base64