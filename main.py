#Módulo para leer archivo CSV. 
from read_file import get_type_file
from libs import get_template, make_folder 
from libs import format_data, save_pdf, qr_code_generate

#Creamos una carpeta donde vamos a guardar los certificados. 
folder_path_save_pdf = make_folder()

#Obtenemos un diccionario con los datos del CSV. 
type_file = get_type_file("CSV")
data_from_file = type_file.read_file("Data.csv")

#Template HTML.
#Obtenemos un objeto de tipo template Jinja2. 
template_html = get_template()


#Iteramos los elementos del diccionario.
for row in data_from_file:
	#Nombre del archivo cuando se guardará.
	nameFile = f"{row['id']} - {row['identification_card']}.pdf"
	
	#newData será los nuevos datos formateados.
	newData = format_data(row)

	#Ubicación y nombre del PDF
	save_file = folder_path_save_pdf + "/" + nameFile

	#Generamos un código QR. 
	text_qr = f"Este Certificado fue emitido por el Centro de Estudio de Comunicación Social y las Tecnologías Libres, CECSOTIL, de la Universidad Bolivariana de Vennezuela a {newData['name']}, cédula de identidad {newData['identification_card']}, por su valiosa participación como {newData['modality']} en: {newData['course']}"
	qr_image_base64 = qr_code_generate(text_qr)

	#Renderizamos un archivo HTML. 
	# Renderiza el template con los datos de la fila actual
	img_css_path = newData['imagen_css_template']
	rendered_template = template_html.render(name=newData['name'], 
											identification_card=newData['identification_card'],
											modality=newData['modality'],
											id=newData['id'],
											date=newData['date'],
											course=newData['course'],
											qr_image_base64=qr_image_base64,
											img_css_path=img_css_path[:-4])

	#Guardamos el certificado PDF.
	save_pdf(img_css_path, rendered_template, save_file)
	
	#Se guarda el archivo PDF. 
	#Imprimimos un progreso.
	print(img_css_path)
	print(f"Certificado: {nameFile} Está listo")


print("Está funcionando")